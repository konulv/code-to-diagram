class Node:
    def __init__(self, line, exits=0, true=None, false=None, code="", special=False):
        self.exits = exits
        self.line = line
        # bad names
        self.true = true
        self.false = false
        self.code = code

    def level(self, level, count):
        self.level = level
        self.count = count

    def __str__(self) -> str:
        true = None if self.true is None else self.true.line
        false = None if self.false is None else self.false.line
        return f"({self.line}) -> ({true}){''if false is None else f' / ({false})'}"
    
    def __repr__(self) -> str:
        return str(self)
    