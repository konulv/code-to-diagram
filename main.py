from node import Node

def find_tab_level(string):
    tabs = ["\t", "    "]
    lvl = 0
    for i in tabs:
        while i in string[:len(i)]:
            lvl += 1
            string = string[len(i):]
    
    return lvl

def main():
    # filename = input("enter a file name ")
    filename = "test_code.py"

    with open(filename, "r") as file:
        content = file.readlines()
        content = [i[:-1] if i[-1] == "\n" else i for i in content] # removing \n
    
    nodes = []
    for i in content:
        print(f"{find_tab_level(i)}, '{i}'")


    
if __name__ == "__main__":
    main()